﻿open System
open System.Collections.Generic

type Instruction = 
    // Util
    | Noop | Show | ShowStack | Info

    // Stack manipulation
    | Swap | Drop | Dup | Rot

    // Arithmetic
    | Add | Sub | Mul | Div | Rem

    // Comparison
    | Eq | Neq | Lt | Lte | Gt | Gte

    // Strings
    | Word | WriteString | StartString | EndString

    // Word flow
    | Call

// TODO: There should be more than just auto-named params
type SigExpr (id: char) =
    member val id = id
    override _.ToString () = Char.ToString id

type Input = SigExpr list
type Output = SigExpr list

// TODO: finish sketching this out
type Signature (input: Input, output: Output) =
    member val input = input
    member val output = output
    override _.ToString () =
        let fmt = List.map (fun x -> x.ToString ()) >> String.concat " "
        sprintf "%s -- %s" (fmt input) (fmt output)
    new () = Signature ([], [])

type Word (name: string, instructions: Instruction array, signature: Signature) =
    member val name = name
    member val instrs = instructions
    member val signature = signature

    override this.ToString() =
        sprintf "%s: %A" this.name this.instrs

    new (name: String, instructions: Instruction array) = 
        Word (name, instructions, Signature ())

/// A pointer into a word in the dictionary.
/// 
/// Words have an address and within words each instruction has an offset.
type CodePtr (index: int, offset: int) =
    member val index = index
    member val offset = offset
    new (index: int) = CodePtr (index, 0)
    override _.ToString () =
        sprintf "[%i,%i]" index offset

type Dictionary (``namespace``: string, words: Word list) =
    let mutable words : Word list = words
    
    new () = Dictionary (String.Empty, List.empty)

    /// Creates a dictionary with the default wordlist loaded.
    static member WithPrelude () =
        Dictionary (String.Empty,
            [ Word ("swap", [| Swap |])
            ; Word (".", [| Show |])
            ; Word (".s", [| ShowStack |])
            ; Word (".info", [| Info |])
            ; Word ("drop", [| Drop |])
            ; Word ("dup", [| Dup |])
            ; Word ("rot", [| Rot |])
            ; Word ("+", [| Add |])
            ; Word ("-", [| Sub |])
            ; Word ("*", [| Mul |])
            ; Word ("/", [| Div |])
            ; Word ("%", [| Rem |])
            ; Word ("=", [| Eq |])
            ; Word ("<>", [| Neq |])
            ; Word ("<", [| Lt |])
            ; Word ("<=", [| Lte |])
            ; Word (">", [| Gt |])
            ; Word (">=", [| Gte |])
            ; Word ("word", [| Instruction.Word |])
            ; Word ("type", [| WriteString |])
            ; Word ("s\"", [| StartString |])
            ; Word ("\"", [| EndString |])
            ])

    member val Namespace: string = ``namespace``

    member _.Define (name, instructions) =
        words <- Word (name, instructions) :: words

    /// Search for a word in this dictionary
    member _.Lookup name = 
        List.tryFind (fun (word: Word) -> word.name = name) words

    member _.IndexOf name =
        words 
        |> List.tryFindIndex (fun (word: Word) -> word.name = name) 
        |> Option.map CodePtr

    override this.ToString () =
        let words = String.concat " " <| List.map (fun word -> word.ToString ()) words
        sprintf "%s %s" this.Namespace words

type Parser () =
    let mutable cursor = 0
    let mutable contents = String.Empty
    member _.Peek () =
        if cursor >= contents.Length then None
        else Some contents.[cursor]
    member _.Advance () =
        cursor <- min contents.Length (cursor + 1)
    member _.Reset (str: string) =
        cursor <- 0
        contents <- str

// TOOD: This sucks and is bad
type StringHeap () =
    let strings = List<string> ()
    member _.Add s =
        let index = strings.Count 
        strings.Add s 
        index
    member _.Get index = strings.[index]
    member _.Remove index = strings.RemoveAt index

type InterpreterMode = Interpreting | Compiling

type InterpreterError =
    | UnrecognizedWord
    | StackUnderflow

exception InterpreterException of InterpreterError

type Interpreter (dictionary: Dictionary) =
    let mutable stack = List.empty
    let mutable rStack: CodePtr list = List.empty
    let mutable mode = Interpreting
    let mutable firstTime = true
    let parser = Parser ()
    let stringHeap = StringHeap ()
    let dictionary = dictionary

    let fetchInput quietly =
        if firstTime then firstTime <- false
        elif quietly then ()
        else printfn "ok"
        parser.Reset <| (Console.ReadLine ()).Trim ()
        printf "  "

    let getNextToken quiet =
        let rec iter acc =
            match parser.Peek () with
            | None ->
                if acc = [] then
                    fetchInput quiet
                    iter acc
                else 
                    acc |> List.rev |> List.map Char.ToString |> List.reduce (+)
            | Some ' ' | Some '\n' ->
                parser.Advance ()
                acc |> List.rev |> List.map Char.ToString |> List.reduce (+)
            | Some c ->
                parser.Advance ()
                iter (c::acc)
        iter []

    let execute = function
        | Noop -> ()
        | Show -> 
            match stack with 
            | [] -> raise (InterpreterException StackUnderflow)
            | hd::tl ->
                printf "%i " hd
                stack <- tl
        | ShowStack -> 
            printf "<%i> %A " stack.Length <| List.rev stack
        | Info ->
            printfn "Stack: <%i> %A" stack.Length <| List.rev stack
            printf "Dict: %A" dictionary
        | Swap -> 
            match stack with
            | fst::snd::rest ->
                stack <- snd::fst::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Drop ->
            match stack with
            | hd::tl -> 
                stack <- tl
            | _ -> raise (InterpreterException StackUnderflow)
        | Dup ->
            match stack with
            | hd::tl -> 
                stack <- hd::hd::tl
            | _ -> raise (InterpreterException StackUnderflow)
        | Rot ->
            match stack with
            | a::b::c::rst ->
                stack <- c::a::b::rst
            | _ -> raise (InterpreterException StackUnderflow)
        | Add ->
            match stack with
            | fst::snd::rest -> 
                stack <- (fst + snd)::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Sub ->
            match stack with
            | fst::snd::rest -> 
                stack <- (snd - fst)::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Mul ->
            match stack with
            | fst::snd::rest -> 
                stack <- (fst * snd)::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Div ->
            match stack with
            | fst::snd::rest -> 
                stack <- (snd / fst)::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Rem ->
            match stack with
            | fst::snd::rest -> 
                stack <- (fst % snd)::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Eq ->
            match stack with
            | fst::snd::rest ->
                let hd = if fst = snd then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Neq ->
            match stack with
            | fst::snd::rest ->
                let hd = if fst <> snd then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Lt ->
            match stack with
            | fst::snd::rest ->
                let hd = if snd < fst then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Lte ->
            match stack with
            | fst::snd::rest ->
                let hd = if snd <= fst then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Gt ->
            match stack with
            | fst::snd::rest ->
                let hd = if snd > fst then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Gte -> 
            match stack with
            | fst::snd::rest ->
                let hd = if snd >= fst then -1 else 0
                stack <- hd::rest
            | _ -> raise (InterpreterException StackUnderflow)
        | Instruction.Word ->
            let token = getNextToken true
            let handle = stringHeap.Add token
            stack <- handle::stack
        | WriteString ->
            match stack with
            | hd::tl ->
                printf "%s " <| stringHeap.Get hd
                stack <- tl
            | _ -> raise (InterpreterException StackUnderflow)
        | StartString -> failwith "Unimplemented!"
        | EndString -> failwith "Unimplemented!"

    member _.Interpret () =
        let token = getNextToken false
        match dictionary.Lookup token with
        | Some word ->
            try
                Array.iter execute word.instrs
                Ok ()
            with InterpreterException err -> Error err
        | None -> 
            match Int32.TryParse token with
            | true, n -> 
                stack <- n :: stack
                Ok ()
            | false, _ -> Error UnrecognizedWord

    member _.Reset () =
        stack <- []
        mode <- Interpreting
        firstTime <- true

type Application () =
    let mutable dictionary = Dictionary.WithPrelude ()
    let mutable interpreter = Interpreter dictionary

    member this.Run () =
        while true do
            match interpreter.Interpret () with
            | Ok () -> ()
            | Error err ->
                printf "Error %A" err
                interpreter.Reset ()

[<EntryPoint>]
let main args = 
    Console.Title <- "PL/7"
    Console.Clear ()
    printfn "PL/7 Interactive Environment..."
    let application = Application ()
    application.Run ()
    0
